import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Highlights from '@/components/Highlights'
import Events from '@/components/Events'
import Team from '@/components/Team'
import Contact from '@/components/Contact'
import Departments from '@/components/Departments'
import DetailsEvents from '@/components/DetailsEvents'
import DetailsGenerals from '@/components/DetailsGenerals'
import DetailsWorkshops from '@/components/DetailsWorkshops'
import Generals from '@/components/Generals'
import Workshops from '@/components/Workshops'
import Accomodation from '@/components/Accomodation'
import Ambassador from '@/components/Ambassador'
import Error from '@/components/404'
import Axios from '@/components/Axios'
import Techpkg from '@/components/Techpkg'
import Logo from '@/components/Logo'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/highlights',
      name: 'Highlights',
      component: Highlights
    },
    {
      path: '/logo',
      name: 'Logo',
      component: Logo
    },
    {
      path: '/axios',
      name: 'Axios',
      component: Axios
    },
    {
      path: '/ambassador/search',
      name: 'Ambassador',
      component: Ambassador
    },
    {
      path: '/workshops',
      name: 'Workshops',
      component: Workshops

    },
    {
      path: '/generals',
      name: 'Generals',
      component: Generals

    },
    {
      path: '/accomodation',
      name: 'Accomodation',
      component: Accomodation

    },
    {
      path: '/events',
      name: 'Events',
      component: Events

    },
    {
      path: '/events/:dept',
      name: 'Departments',
      component: Departments

    },
    {
      path: '/events/details/:dept/:ecode',
      name: 'DetailsEvents',
      component: DetailsEvents

    },
    {
      path: '/generals/:ecode',
      name: 'DetailsGenerals',
      component: DetailsGenerals

    },
    {
      path: '/workshops/:ecode',
      name: 'DetailsWorkshops',
      component: DetailsWorkshops

    },
    {
      path: '/team',
      name: 'Team',
      component: Team

    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact

    },
    {
      path: '/technical/package',
      name: 'Techpkg',
      component: Techpkg

    },
    {
      path: '*',
      name: 'Error',
      component: Error
    }
  ]
})
